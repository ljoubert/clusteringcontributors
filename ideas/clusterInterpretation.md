# Interprétation des clusters

Version du 16 avril 2024 : 3 clusters ont été identifiés. Les clusters sont définis par les caractéristiques suivantes :

- **Classe 1 : trop occupés pour contribuer**. Ces individus pratiques de nombreuses autres activités et contribuent peu durant une faible quantité de temps, ainsi qu'à discuter de la contribution.
- **Classe 2 : les parleurs**. Ces individus discutent beaucoup de Wikipédia avec les autres, mais contribuent peu et se livrent peu à d'autres activités.
- **Classe 3 : les gros contributeurs**. Ces individus contribuent beaucoup, de façon diverses sur de longues plages de temps. Ils discutent modérément de Wikipédia avec les autres.