# Files

- clusterContributors.R : making of clusters and analysis
- data_filtered.csv : filtering of the database (coming from datapaper)
- interets.csv : variables of interest + new naming conventions
- visualizationHelpers.R : convenient functions to plot MCA (variables and indiviuals)

# TODO

- inspecting inconsistent answers (for instance : contributors that doesn't contribute neither weeksdays not week-ends)
- adding variables to complete clusters
- more cleaning on time group
- make more informative plots about clustering
- explore more adapted clustering strategies (with, for instance, weighted method analog to pure MFA of better clustering algorithm)
- finding more readable Word and PPT export